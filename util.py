import optparse
import time
import pickle

import constants


def parseArguments(argv):
    """
    Parse argument options and returns option list

    :param argv: program arguments
    :return: option list
    """
    parser = optparse.OptionParser()
    parser.add_option('-r', '--resnet',
                      action="store", dest="resnet_type",
                      help="ResNet Type (can be: 'resnet18', 'resnet50' .. etc.)",
                      default=constants.DEFAULT_RESNET_TYPE)
    parser.add_option('-m', '--model',
                      action="store", dest="model",
                      help="Model (kNN or SVM)",
                      default=constants.DEFAULT_MODEL)
    parser.add_option('-n', '--neighbors',
                      action="store", dest="n_neighbors",
                      help="Number of Neighbors (integer)",
                      default=constants.DEFAULT_N_NEIGHBORS)
    options, args = parser.parse_args(argv)
    return options


def writeLog(tag, log):
    """
    Writes logs to console log file.

    :param tag: log tag
    :param log: log message
    """
    f = open(constants.LOG_FILE, "a")
    start = time.strftime("%H:%M:%S") + "   LOG/ " + tag.upper() + ":  "
    log = log.replace("\n", "\n" + (" " * len(start)))
    buffer = start + log + "\n"
    f.write(buffer)
    f.close()


def writeAsSerializable(array, fileName):
    """
    Writes array to file as serializable.

    :param array: array to dump
    :param fileName: which file?
    """
    file = open(fileName, 'wb')
    pickle.dump(array, file)
    file.close()
    writeLog("PICKLE", "PICKLE file is created as " + fileName)


def getObjectFromPickle(fileName):
    """
    Gets array from pickle file.

    :param fileName: file that contains binary values of array
    :return: an array from given file
    """
    file = open(fileName, 'rb')
    array = pickle.load(file)
    file.close()
    writeLog("PICKLE", "Array is get from pickle file " + fileName)
    return array
