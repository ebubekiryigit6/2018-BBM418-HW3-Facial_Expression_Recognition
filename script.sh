#!/usr/bin/env bash

# resnet types svm
python3.6 ./main.py -r resnet50 -m svm -n 33
python3.6 ./main.py -r resnet101 -m svm -n 33
python3.6 ./main.py -r resnet152 -m svm -n 33


# neighbor
python3.6 ./main.py -r resnet50 -m knn -n 20
python3.6 ./main.py -r resnet50 -m knn -n 30
python3.6 ./main.py -r resnet50 -m knn -n 35
python3.6 ./main.py -r resnet50 -m knn -n 40
python3.6 ./main.py -r resnet50 -m knn -n 45


# resnet types knn
python3.6 ./main.py -r resnet50 -m knn -n 33
python3.6 ./main.py -r resnet101 -m knn -n 33
python3.6 ./main.py -r resnet152 -m knn -n 33

