import sys
import time

import numpy as np

import constants
import util
from vision_engine import DataLoader, SpatialFeatureExtractor, OpticalFeatureExtractor, kNearestNeighbors, SVM


def main(argv):
    options = util.parseArguments(argv)
    resnet_type = options.resnet_type
    model = options.model
    n_neighbors = int(options.n_neighbors)

    util.writeLog("ARG-PARSER", "Argument Informations:"
                  + "\nRESNET TYPE:     " + options.resnet_type
                  + "\nMODEL:           " + options.model
                  + "\nNUM OF NEIGHBOR: " + options.n_neighbors)

    # load test and train data
    train_data_loader = DataLoader(constants.TRAIN_IMAGES_PATH)
    test_data_loader = DataLoader(constants.TEST_IMAGES_PATH)

    # extract optical and spatial features
    opt_ft_train = OpticalFeatureExtractor(train_data_loader.class_to_group)
    sp_ft_train = SpatialFeatureExtractor(train_data_loader.class_to_group, resnet_type)

    op_ft_test = OpticalFeatureExtractor(test_data_loader.class_to_group)
    sp_ft_test = SpatialFeatureExtractor(test_data_loader.class_to_group, resnet_type)

    # get feature and label lists
    opt_train_features = opt_ft_train.get_feature_list()
    opt_test_features = op_ft_test.get_feature_list()
    opt_train_labels = opt_ft_train.label_list
    opt_test_labels = op_ft_test.label_list

    sp_train_features = sp_ft_train.get_feature_list()
    sp_test_features = sp_ft_test.get_feature_list()
    sp_train_labels = sp_ft_train.label_list
    sp_test_labels = sp_ft_test.label_list

    # evaluate
    if model.lower() == constants.KNN.lower():
        knn_optical = kNearestNeighbors(n_neighbors, opt_train_features, opt_test_features,
                                        opt_train_labels, opt_test_labels)
        knn_spatial = kNearestNeighbors(n_neighbors, sp_train_features, sp_test_features,
                                        sp_train_labels, sp_test_labels)

        util.writeLog("RESULT", "KNN Optical Accuracy: " + str(knn_optical.get_accuracy()))
        util.writeLog("RESULT", "KNN Spatial Accuracy: " + str(knn_spatial.get_accuracy()))
    elif model.lower() == constants.SVM.lower():
        svm_optical = SVM(opt_train_features, opt_test_features,
                          opt_train_labels, opt_test_labels)
        svm_spatial = SVM(sp_train_features, sp_test_features,
                          sp_train_labels, sp_test_labels)
        util.writeLog("RESULT", "SVM Optical Accuracy: " + str(svm_optical.get_accuracy()))
        util.writeLog("RESULT", "SVM Spatial Accuracy: " + str(svm_spatial.get_accuracy()))
    else:
        if len(opt_train_labels) != len(sp_train_features):
            util.writeLog("MAIN-PROCESS", "Temporal and Spatial features list's length is not same.\nClosing...")
            exit(1)
        else:
            train_features_combined = np.concatenate((opt_train_features, sp_train_features), axis=1)
            test_features_combined = np.concatenate((opt_test_features, sp_test_features), axis=1)

            # normalize features (provides %2-4 accuracy)
            x = np.array(train_features_combined)
            y = np.array(test_features_combined)
            normalized_train = (x - np.min(x)) / (np.max(x) - np.min(x))
            normalized_test = (y - np.min(y)) / (np.max(y) - np.min(y))

            svm_all = SVM(normalized_train, normalized_test, sp_train_labels, sp_test_labels)
            util.writeLog("RESULT", "SVM All Accuracy: " + str(svm_all.get_accuracy()))

            knn_all = kNearestNeighbors(n_neighbors, normalized_train, normalized_test, sp_train_labels, sp_test_labels)
            util.writeLog("RESULT", "KNN All Accuracy: " + str(knn_all.get_accuracy()))


if __name__ == "__main__":
    util.writeLog("MAIN-PROCESS", "Facial Expression Recognition program is started.")
    start = time.time()
    main(sys.argv)
    util.writeLog("MAIN-PROCESS",
                  "Facial Expression Recognition program is finished in "
                  + str(time.time() - start) + " seconds.")
