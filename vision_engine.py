import cv2
import numpy as np
from PIL import Image
from sklearn import svm
from sklearn.neighbors import KNeighborsClassifier
from torch import nn
from torch.autograd import Variable
from torchvision import transforms, models
import os
import collections

import constants
import util


class DataLoader:

    def __init__(self, dataset_path):
        self.dataset_path = dataset_path
        self.__load_dataset()

    def __load_dataset(self):
        self.__load_images()

    def __load_images(self):
        self.__get_images(self.dataset_path)
        util.writeLog("DATA-LOADER", self.dataset_path + " is loaded by Data Loader.")

    def __get_images(self, path):
        filePathList = []
        all_group_images = []
        class_to_group_images = {}
        class_to_idx = {}
        k = 0
        for label_file in os.listdir(path):
            label_path = os.path.join(path, label_file)
            # ignore neutral class
            if label_file != constants.NEUTRAL_CLASS_LABEL:
                for id_file in os.listdir(label_path):
                    id_file_path = os.path.join(label_path, id_file)
                    for four_object in os.listdir(id_file_path):
                        group_path = os.path.join(id_file_path, four_object)
                        group_images = []
                        for image in os.listdir(group_path):
                            full_image_path = os.path.join(group_path, image)
                            filePathList.append(full_image_path)
                            group_images.append(full_image_path)
                        all_group_images.append(group_images)
                        if k in class_to_group_images:
                            class_to_group_images[k].append(group_images)
                        else:
                            class_to_group_images[k] = [group_images]
            # else:
            #     for neutral_file in os.listdir(label_path):
            #         neutral_file_path = os.path.join(label_path, neutral_file)
            #         filePathList.append(neutral_file_path)
            class_to_idx[label_file] = k
            k += 1
        filePathList.sort()
        self.all_group_images = all_group_images
        self.all_image_list = filePathList
        # order dictionary 0 to N
        self.class_to_group = collections.OrderedDict(sorted(class_to_group_images.items(), key=lambda t: t[0]))


class FaceDetector:
    __faces = []

    def __init__(self, image):
        self.image = image
        self.__faces = []

    def detect(self):
        face_cascade = cv2.CascadeClassifier(constants.FACE_DETECTOR_XML)
        img = self.image
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        faces = face_cascade.detectMultiScale(gray, 1.3, 5)
        # get face rectangles
        for (x, y, w, h) in faces:
            crop_img = img[y:y + h, x:x + w]
            self.__faces.append(crop_img)
        return self

    def get_face(self):
        if self.is_empty():
            return self.image
        else:
            return self.__faces[0]

    def is_empty(self):
        return len(self.__faces) == 0

    def __len__(self):
        return len(self.__faces)


class OpticalFlow:
    __features = []
    __mag_feature = []
    __ang_feature = []

    def __init__(self, first_image, second_image, resize=224):
        self.first_image = first_image
        self.second_image = second_image
        self.__features = []
        self.__mag_feature = []
        self.__ang_feature = []
        self.__resize = resize

    def execute(self):
        frame1 = self.first_image
        frame1 = cv2.resize(frame1, (self.__resize, self.__resize))
        prvs = cv2.cvtColor(frame1, cv2.COLOR_BGR2GRAY)
        hsv = np.zeros_like(frame1)
        hsv[..., 1] = 255

        frame2 = self.second_image
        frame2 = cv2.resize(frame2, (self.__resize, self.__resize))
        next = cv2.cvtColor(frame2, cv2.COLOR_BGR2GRAY)

        flow = cv2.calcOpticalFlowFarneback(prvs, next, None, 0.5, 3, 15, 3, 5, 1.2, 0)
        mag, ang = cv2.cartToPolar(flow[..., 0], flow[..., 1])

        hsv[..., 0] = ang * 180 / np.pi / 2
        hsv[..., 2] = cv2.normalize(mag, None, 0, 255, cv2.NORM_MINMAX)
        bgr = cv2.cvtColor(hsv, cv2.COLOR_HSV2BGR)

        # define angular and magnitude feature
        ang_feature = bgr[..., 0].flatten()
        mag_feature = bgr[..., 2].flatten()

        self.__mag_feature = mag_feature
        self.__ang_feature = ang_feature

        return self
        # cv2.imshow('frame2', bgr)
        # cv2.waitKey()

    def get_magnetude_feature(self):
        return self.__mag_feature

    def get_angular_feature(self):
        return self.__ang_feature


RESNET18 = "resnet18"
RESNET34 = "resnet34"
RESNET50 = "resnet50"
RESNET101 = "resnet101"
RESNET152 = "resnet152"


class ResNet:
    __feature = []
    model = None

    def __init__(self, image, resize=224):
        self.image = image
        self.__feature = []
        self.model = RESNET50
        self.resize = resize

    def set_model(self, model=RESNET50):
        self.model = model
        return self

    def execute(self):
        scaler = transforms.Resize((self.resize, self.resize))
        normalizer = transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])
        to_tensor = transforms.ToTensor()

        if self.model.lower() == RESNET18.lower():
            pre_train_model = models.resnet18(pretrained=True)
        elif self.model.lower() == RESNET34.lower():
            pre_train_model = models.resnet34(pretrained=True)
        elif self.model.lower() == RESNET101.lower():
            pre_train_model = models.resnet34(pretrained=True)
        elif self.model.lower() == RESNET152.lower():
            pre_train_model = models.resnet34(pretrained=True)
        else:
            pre_train_model = models.resnet50(pretrained=True)

        pre_train_model = nn.Sequential(*list(pre_train_model.children())[:-1])
        pre_train_model.eval()

        # no train no pain but gain :)
        for p in pre_train_model.parameters():
            p.requires_grad = False

        tensor_img = Variable(normalizer(to_tensor(scaler(self.image))).unsqueeze(0))
        preds = pre_train_model(tensor_img)

        self.__feature = (preds.data.numpy()[0]).flatten()
        # print(self.__feature)
        return self

    def get_feature(self):
        return self.__feature


class OpticalFeatureExtractor:
    def __init__(self, class_to_path_dict):
        self.class_to_path_dict = class_to_path_dict
        self.__extract_features()

    def __extract_features(self):
        util.writeLog("Feature-Extractor", "Optical Feature extraction is started.")
        class_to_features = {}
        label_list = []
        for label, groups in self.class_to_path_dict.items():
            for group in groups:
                step = len(group) - 1
                optical_features = []
                # group features of optical frames
                for i in range(step):
                    first_image_path = group[i]
                    second_image_path = group[i + 1]
                    first_image = cv2.imread(first_image_path)
                    second_image = cv2.imread(second_image_path)

                    first_image_face = FaceDetector(first_image).detect().get_face()
                    second_image_face = FaceDetector(second_image).detect().get_face()
                    optical_flow = OpticalFlow(first_image_face, second_image_face).execute()

                    optical_features.extend(optical_flow.get_magnetude_feature())
                label_list.append(label)
                if label in class_to_features:
                    class_to_features[label].append(optical_features)
                else:
                    class_to_features[label] = [optical_features]
        self.class_to_features = class_to_features
        self.label_list = label_list
        util.writeLog("Feature-Extractor", "Optical Feature extraction is finished.")

    def get_feature_list(self):
        features = []
        for i in self.class_to_features.values():
            features.extend(i)
        return features


class SpatialFeatureExtractor:
    def __init__(self, class_to_path_dict, resnet_type):
        self.class_to_path_dict = class_to_path_dict
        self.resnet_type = resnet_type
        self.__extract_features()

    def __extract_features(self):
        util.writeLog("Feature-Extractor", "Spatial Feature extraction is started.")
        class_to_features = {}
        label_list = []
        for label, groups in self.class_to_path_dict.items():
            for group in groups:
                spatial_features = []
                for image_path in group:
                    # get spatial feature with resnet model
                    resnet = ResNet(Image.open(image_path).convert('RGB')).set_model(self.resnet_type).execute()
                    spatial_features.extend(resnet.get_feature())
                if label in class_to_features:
                    class_to_features[label].append(spatial_features)
                else:
                    class_to_features[label] = [spatial_features]
                label_list.append(label)
        self.class_to_features = class_to_features
        self.label_list = label_list
        util.writeLog("Feature-Extractor", "Spatial Feature extraction is finished.")

    def get_feature_list(self):
        features = []
        for i in self.class_to_features.values():
            features.extend(i)
        return features


class kNearestNeighbors:
    def __init__(self, n_neighbors, train_data, test_data, train_labels, test_labels):
        self.n_neighbors = n_neighbors
        self.train_data = train_data
        self.test_data = test_data
        self.train_labels = train_labels
        self.test_labels = test_labels
        self.__execute()

    def __execute(self):
        util.writeLog("KNN-MODEL", "kNN is started with " + str(self.n_neighbors) + " n_neighbors.")
        knn = KNeighborsClassifier(self.n_neighbors)
        knn.fit(self.train_data, self.train_labels)
        self.results = knn.predict(self.test_data)
        self.__calculate_accuracy()

    def __calculate_accuracy(self):
        success_count = 0.0
        for i, j in zip(self.results, self.test_labels):
            if i == j:
                success_count += 1
        self.accuracy = success_count / len(self.results)
        util.writeLog("KNN-MODEL", "kNN is finished.")

    def get_accuracy(self):
        return self.accuracy


class SVM:
    def __init__(self, train_data, test_data, train_labels, test_labels):
        util.writeLog("SVM-MODEL", "SVM is started.")
        self.train_data = train_data
        self.test_data = test_data
        self.train_labels = train_labels
        self.test_labels = test_labels
        self.__execute()

    def __execute(self):
        clf = svm.SVC(decision_function_shape='ovo')
        clf.fit(self.train_data, self.train_labels)
        self.results = clf.predict(self.test_data)
        self.__calculate_accuracy()
        util.writeLog("SVM-MODEL", "SVM is finished.")

    def __calculate_accuracy(self):
        success_count = 0.0
        for i, j in zip(self.results, self.test_labels):
            if i == j:
                success_count += 1
        self.accuracy = success_count / len(self.results)

    def get_accuracy(self):
        return self.accuracy
