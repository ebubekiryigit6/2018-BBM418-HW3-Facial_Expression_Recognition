# Facial Expression Recognition

BBM418 - Computer Vision Lab.

Assignment 3

### Author
Ebubekir Yiğit - 21328629



### Project Introduction
In this project, we will find the temporal and spatial features of the images in the given data. 
Then we will calculate the accuracy by giving it to models like KNN, SVM. 
We will use Optical Flow to find the changes 
between the 4 pictures given when the Temporal feature is found. 
For this, first we will find the faces in the images by doing 
Face Detection and we will make the changes between them as features. 
By combining the features we find, we will create a feature of the group. 
When creating Spatial Feature, we will make official models and 
features according to the ResNet type coming from the user. 
We will not train the model. We will compare these models with SVM and KNN and 
their accuracy by trying their features together. We will add features such as mouth, 
eye to enhance accuracy. We will normalize the features. 
We were going to use PCA but we did not have time.


## Getting Started

You can browse the "script" files to run the entire process of the project.

I can not submit because the Submit system does not support cmd and sh files :)

For Windows:
```bash
    > script.cmd
```

For Linux:
```bash
    > sh script.sh
```


**All operations are progressively saved to the console.log file. 
You can browse the console.log file to see the operations.**




### Prerequisites

Python 3.6 is required to run the project. 

Python External Libraries:

    PyTorch 0.3.1 - TorchVision - Numpy - Pillow - OpenCV - Scikit
    
If you are having trouble installing PyTorch on Microsoft Windows:

-   Note that your "pip" version is 10.0 and above.
-   Install Anaconda 2 or Anaconda 3
-   Open your Anaconda Prompt as Administrator.
-   Run 'conda create -n vision_env python=3.6 numpy pyyaml mkl' to create your environment
-   Run 'conda activate vision_env' to activate environment
-   Run 'pip install --upgrade pip' to update pip
-   Run 'conda install -c peterjc123 pytorch' to install PyTorch (This command installs latest version of Pytorch if you want to install different version please use pytorch='0.3.1')
-   Run 'pip install docutils pygments pypiwin32 kivy.deps.sdl2 kivy.deps.glew'
-   Run 'pip install kivy.deps.gstreamer'
-   If it doesnt help, please see 'https://www.superdatascience.com/pytorch/'


If you are having trouble installing OPENCV on Microsoft Windows:

-   Note that your "pip" version is 9.0 and above.
-   Open your Anaconda Prompt as Administrator.
-   Run 'conda activate vision_env' to activate environment
-   Run 'pip install opencv-python'



For Face Detection OpenCV requires face xml files.
You should download it:
-   https://github.com/opencv/opencv/tree/master/data/haarcascades


### Running the Project

    python3.6 main.py [options]
    
    Options:
        -r <resnet_type> (supports all resnet types in PyTorch: resnet18 to resnet152)
        -m <model> (SVM, KNN or both)
        -n <n_neighbors> (Number of neighbors of KNN)
        
    Default Options: (If you dont write any options)
        -r resnet50
        -m knn
        -n 33


#### USAGE

To Run with default configs:
```
python3.6 main.py
```
-   Program uses: ResNet50 - SVM - 33

-   Output: Information of process and model accuracy.



To Run with your own configs:

    python3.6 main.py -r resnet152 -m both -n 40
    
-   Output: Information of process and model accuracy.


**After run, please see "console.log" file to show process logs.**

### console.log File
```text
05:51:18   LOG/ MAIN-PROCESS:  Facial Expression Recognition program is started.
05:51:18   LOG/ ARG-PARSER:  Argument Informations:
                             RESNET TYPE:     resnet152
                             MODEL:           svm
                             NUM OF NEIGHBOR: 33
05:51:18   LOG/ DATA-LOADER:  ./DATASET/DATA/TRAIN is loaded by Data Loader.
05:51:18   LOG/ DATA-LOADER:  ./DATASET/DATA/TEST is loaded by Data Loader.
05:51:18   LOG/ FEATURE-EXTRACTOR:  Optical Feature extraction is started.
05:53:23   LOG/ FEATURE-EXTRACTOR:  Optical Feature extraction is finished.
05:53:23   LOG/ FEATURE-EXTRACTOR:  Spatial Feature extraction is started.
06:14:14   LOG/ FEATURE-EXTRACTOR:  Spatial Feature extraction is finished.
06:14:14   LOG/ FEATURE-EXTRACTOR:  Optical Feature extraction is started.
06:14:43   LOG/ FEATURE-EXTRACTOR:  Optical Feature extraction is finished.
06:14:43   LOG/ FEATURE-EXTRACTOR:  Spatial Feature extraction is started.
06:19:31   LOG/ FEATURE-EXTRACTOR:  Spatial Feature extraction is finished.
06:21:04   LOG/ PICKLE:  PICKLE file is created as pickle/resnet152_svm_33/optical_train_feature.bin
06:21:22   LOG/ PICKLE:  PICKLE file is created as pickle/resnet152_svm_33/optical_test_feature.bin
06:21:23   LOG/ PICKLE:  PICKLE file is created as pickle/resnet152_svm_33/spatial_train_feature.bin
06:21:23   LOG/ PICKLE:  PICKLE file is created as pickle/resnet152_svm_33/spatial_test_feature.bin
06:21:23   LOG/ PICKLE:  PICKLE file is created as pickle/resnet152_svm_33/train_labels.bin
06:21:23   LOG/ PICKLE:  PICKLE file is created as pickle/resnet152_svm_33/test_labels.bin
06:21:23   LOG/ SVM-MODEL:  SVM is started.
06:21:55   LOG/ SVM-MODEL:  SVM is finished.
06:21:55   LOG/ SVM-MODEL:  SVM is started.
06:21:56   LOG/ SVM-MODEL:  SVM is finished.
06:21:56   LOG/ RESULT:  SVM Optical Accuracy: 0.2459016393442623
06:21:56   LOG/ RESULT:  SVM Spatial Accuracy: 0.4918032786885246
06:21:56   LOG/ MAIN-PROCESS:  Facial Expression Recognition program is finished in 1838.76132106781 seconds.
```
